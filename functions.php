<?php

include 'includes/config.php';
include 'includes/db.inc.php';

function db_connect() {
    return new MySQLdb(DB_HOST,DB_USERNAME,DB_PASSWORD,false,DB_NAME);
}

function getListDevices() {
    $db = db_connect();

    $devices_query = $db->query('SELECT * FROM `devices`');

    return $db->mysql_fetch_all($devices_query);
}

function getBaseUrl() {
    // output: /myproject/index.php
    $currentPath = $_SERVER['PHP_SELF'];

    // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
    $pathInfo = pathinfo($currentPath);

    // output: localhost
    $hostName = $_SERVER['HTTP_HOST'];

    // output: http://
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';

    // return: http://localhost/myproject/
    return $protocol.'://'.$hostName.$pathInfo['dirname']."/";
}

function getAjaxUrl() {
    return getBaseUrl() . 'ajax.php';
}

function ajax_getListDevices() {
    $html = '';

    $list_devices = getListDevices();

    ob_start();

    include 'templates/parts/list-devices.php';
    $html = ob_get_contents();
    ob_end_clean();

    die($html);
}