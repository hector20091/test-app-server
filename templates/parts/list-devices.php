<table border="1">
    <thead>
    <tr>
        <th>DB ID</th>
        <th>Device ID</th>
        <th>Public IP</th>
        <th>Date Time</th>
    </tr>
    </thead>
    <tbody>

    <?php if(sizeof($list_devices) > 0) {
        foreach ($list_devices as $device_data) { ?>

            <tr>
                <td><?php echo $device_data['id']; ?></td>
                <td><?php echo $device_data['device_id']; ?></td>
                <td><?php echo $device_data['public_ip']; ?></td>
                <td><?php echo $device_data['datetime']; ?></td>
            </tr>

        <?php }
    } else { ?>

        <tr>
            <td colspan="3">No Active Devices</td>
        </tr>

    <?php } ?>

    </tbody>
</table>