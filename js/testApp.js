var TestApp = function () {
    var dynamicDevicesListTableId = '#dynamicDevicesListTable';

    return {
        init: function () {
            TestApp.listDevices();

            setInterval(function () {
                TestApp.listDevices();
            }, 5000);
        },

        listDevices: function () {
            $.ajax({
                url: config.ajaxUrl,
                data: {
                    action: 'getListDevices'
                }
            }).done(function( res ) {
                $(dynamicDevicesListTableId).html(res);
            });
        }
    }
}();

$(window).on('load', function () {
    TestApp.init();
});

