<?php

include "../includes/config.php";
include "../includes/db.inc.php";

class API {

    private $api_url = '';
    private $mysql;
    private $data;

    function __construct() {
        $this->mysql = new MySQLdb(DB_HOST,DB_USERNAME,DB_PASSWORD,false,DB_NAME);
    }

    private function send($result) {
        die(json_encode($result));
    }

    public function connect() {
        $result = array();
        $data = $this->data;

        $sql = "INSERT INTO `devices` (`id`, `device_id`, `public_ip`, `datetime`) 
                VALUES (NULL, '".$data->device_id."', '".$data->public_ip."', '".date("Y-m-d H:i:s")."');";
        $this->mysql->query($sql);

        $result['status'] = true;
        $result['id'] = mysql_insert_id($this->mysql->link_id);

        $this->send($result);
    }

    public function setData( $data ) {
        $this->data = $data;
    }

    public function disconnect() {
        $result = array();
        $data = $this->data;
        $id = $data->id;

        $this->mysql->query("DELETE FROM `devices` WHERE `id` = " . $id);

        $result['status'] = true;

        $this->send($result);
    }


}

$api = new API();