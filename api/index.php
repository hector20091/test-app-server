<?php
/*header("Access-Control-Allow-Orgin: *");
header("Access-Control-Allow-Methods: *");
header("Content-Type: application/json");
header("Accept: application/json");*/

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$data = json_decode(file_get_contents("php://input"));

include 'class.api.php';

if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])) {

    $api->setData($data);

    switch ($_REQUEST['action']) {
        case 'connect':
            $api->connect();
            break;

        case 'disconnect':
            $api->disconnect();
            break;
    }
}